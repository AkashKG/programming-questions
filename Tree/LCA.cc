#include<iostream>
#include<malloc.h>
using namespace std;

typedef struct Node{
    int data;
    struct Node *left, *right;
}*node;

node insertIntoTree(node root, int data){
    if(root==NULL){
        node newnode = (node)malloc(sizeof(struct Node));
        newnode->data = data;
        newnode->left = newnode->right = NULL;
        root = newnode;
        return root;
    }
    if(root->data > data){
        root->left = insertIntoTree(root->left, data);
    }
    else{
        root->right = insertIntoTree(root->right, data);
    }
    return root;
}

void displayInorder(node root){
    if(root==NULL)
        return;
    displayInorder(root->left);
    cout<<root->data<<" ";
    displayInorder(root->right);
}

node LCA(node root, int n1, int n2){
    while (root != NULL){
        if (root->data > n1 && root->data > n2)
           root = root->left;
        else if (root->data < n1 && root->data < n2)
           root = root->right;
        else break;
    }
    return root;
}

int main(){
    node root = NULL;
    int data;
    bool choice = true;
    while(choice){
        cin>>data;
        root = insertIntoTree(root, data);
        cout<<"\nEnter Choice (0|1) : ";
        cin>>choice;
    }
    displayInorder(root);
    cout<<"Enter the two elements to check common Ancestor : ";
    int pred1, pred2;
    cin>>pred1>>pred2;
    node temp;
    if((temp=LCA(root, pred1, pred2))==NULL)
        cout<<"\nNot Possibru!";
    else
        cout<<"\nPossibru!! : "<<temp->data;
    return 0;
}
