#include<iostream>
#include<queue>
#include<malloc.h>
using namespace std;

typedef struct Node{
    int data;
    struct Node *left, *right;
}*node;

node insertIntoTree(node root, int data){
    if(root==NULL){
        node newnode = (node)malloc(sizeof(struct Node));
        newnode->data = data;
        newnode->left = newnode->right = NULL;
        root = newnode;
        return root;
    }
    if(root->data > data){
        root->left = insertIntoTree(root->left, data);
    }
    else{
        root->right = insertIntoTree(root->right, data);
    }
    return root;
}

void displayInorder(node root){
    if(root==NULL)
        return;
    displayInorder(root->left);
    cout<<root->data<<" ";
    displayInorder(root->right);
}

bool complete(node root, int index, int nodes){
    if(root==NULL)
        return true;
    if(index > nodes)
        return false;
    return complete(root->left,2*index+1,nodes) && complete(root->right,2*index+2,nodes);
}

int countNodes(node root){
    if(root == NULL)
        return 0;
    return 1 + countNodes(root->left) + countNodes(root->right);
}

bool completeIter(node root){
    if(root==NULL)
        return true;
    queue<node> q;
    q.push(root);
    while(!q.empty()){
        node front = q.front();
        q.pop();
        if(front->left && front->right){
            q.push(front->left);
            q.push(front->right);
        }
        else if(front->left == NULL && front->right==NULL){
            break;
        }
        else{
            return false;
        }
    }
    while(!q.empty()){
        node front = q.front();
        q.pop();
        if(front->left !=NULL || front ->right !=NULL )
            return false;
    }
    return true;
}

int main(){
    node root = NULL;
    int data;
    bool choice = true, index = 0;
    while(choice){
        cin>>data;
        root = insertIntoTree(root, data);
        cout<<"\nEnter Choice (0|1) : ";
        cin>>choice;
    }
    int count = countNodes(root);
    displayInorder(root);
    if(complete(root,index,count))
        cout<<"Complete Binary Tree";
    else
        cout<<"Not complete Binary Tree";
    if(completeIter(root))
        cout<<"\nComplete Binary Tree";
    else
        cout<<"\nNot complete Binary Tree";
    return 0;
}
