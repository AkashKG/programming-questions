#include<iostream>
#include<malloc.h>
#include<stack>
using namespace std;

typedef struct Node{
    int data;
    Node *left, *right;
}*node;

node insertIntoTree(int data, node root){
    if(root == NULL){
        node newNode = (node)malloc(sizeof(struct Node));
        newNode->data = data;
        newNode->left = NULL;
        newNode->right = NULL;
        root=newNode;
        return root;
    }
    if(data<root->data)
        root->left = insertIntoTree(data, root->left);
    else if(root->data == data){}
    else
        root->right = insertIntoTree(data,root->right);
    return root;
}

void printInorderRec(node root){
    if(root==NULL)
        return;
    printInorderRec(root->left);
    cout<<root->data<<" ";
    printInorderRec(root->right);
}

void printInorderStack(node root){
    if(root == NULL)
        return;
    stack <node> s;
    node current = root;
    while(!s.empty()||current!=NULL){
        if(current!=NULL)
            s.push(current);
        while(current!=NULL){
            current = current->left;
            if(current!=NULL)
                s.push(current);
        }
        current = s.top();
        cout<<current->data<<" ";
        s.pop();
        current=current->right;
    }
}

int main(){
    node root = NULL;
    int opt=6, data;
    while(opt!=5){
        cin>>opt;
        switch(opt){
        case 1:
            cin>>data;
            root = insertIntoTree(data,root);
            break;
        case 2:
            printInorderRec(root);
            break;
        case 3:
            printInorderStack(root);
            break;
        }
    }
}
