#include<iostream>
#include<algorithm>
#include<list>
#include<unordered_map>
using namespace std;

void referencePage(list<int> &L, unordered_map<int, int> &hash, int page){
    if(L.size()<4){
        L.push_back(page);
        hash[page] = page;
        return;
    }
    list<int> :: iterator iter = find(L.begin(), L.end(), page);
    if(iter!=L.end()){
        int temp = *iter;
        L.erase(iter);
        L.push_back(page);
    }
    else{
        hash.erase(L.front());
        L.erase(L.begin());
        L.push_back(page);
        hash[page]=page;
    }
}

int main(void){
    list<int> L;
    unordered_map<int, int> hash;
    referencePage(L,hash,1);
    referencePage(L,hash,2);
    referencePage(L,hash,3);
    referencePage(L,hash,1);
    referencePage(L,hash,4);
    referencePage(L,hash,5);
    for (auto& val : L)
        cout << val << endl;
    return 0;
}
