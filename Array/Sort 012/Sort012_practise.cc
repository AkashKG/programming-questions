#include<iostream>
using namespace std;

void swap(int &a, int &b){
    a = a + b;
    b = a - b;
    a = a - b;
}

void sort012(int arr[], int N){
    int low = 0, mid = 0, high = N-1;
    while(mid<high){
        switch(arr[mid]){
        case 0:
            swap(arr[mid], arr[low]);
            mid++,low++;
            break;
        case 1:
            mid++;
            break;
        case 2:
            swap(arr[mid], arr[high]);
            high--;
            break;
        }
    }
}

void input(int arr[], int N){
    for(int i=0;i<N;i++)
        cin>>arr[i];
}

void display(int arr[], int N){
    for(int i=0;i<N;i++)
        cout<<arr[i]<<" ";
}

int main(){
    int N;
    cin>>N;
    int arr[N];
    input(arr,N);
    sort012(arr,N);
    display(arr,N);
    return 0;
}
