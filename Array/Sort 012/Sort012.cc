#include<iostream>
using namespace std;

void swap(int &a, int &b){ a = a ^ b; b = a ^ b; a = a ^ b;}

void sort012(int arr[], int len){
    int mid = 0, low = 0, high = len;
    while(mid<=high){
        switch(arr[mid]){
        case 0:
            swap(arr[low],arr[mid]);
            low+=1;mid+=1;
            break;
        case 1:
            mid+=1;
            break;
        case 2:
            swap(arr[mid], arr[high]);
            high-=1;
            break;
        }
    }
}

int main(){
    int arr[]={2,2,2,2,2,2,2,2,2,0,0,1,0,0,2,1,1,0,0,1,2,0,0,0,0,0,1};
    int len = sizeof(arr)/sizeof(arr[0]);
    sort012(arr,len-1);
    for(int i=0;i<len;i++){cout<<arr[i]<<" ";}
    return 0;
}
