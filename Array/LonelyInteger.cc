#include <iostream>
#include <cstring>
using namespace std;

/*int MAP[100];

int main() {
    memset(MAP,0,sizeof(MAP));
    int N,M;
    cin>>N;
    while(N--){
        cin>>M;
        MAP[M]+=1;
    }
    for(int i=0;i<100;i++){
        if(MAP[i]==1){
            cout<<i;
            break;
        }
    }
    return 0;
}
*/
int main(){
    int N, M, XOR=0;
    cin>>N;
    while(N--){
        cin>>M;
        XOR = M^XOR;
    }
    cout<<XOR;
}
