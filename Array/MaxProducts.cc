#include<iostream>
using namespace std;

int maxProd(int arr[], int len){
    int minEndingHere = 1, maxEndingHere = 1, maxTillHere = 0;
    for(int i=0;i<len;i++){
        if(arr[i]>0){
            maxEndingHere*=arr[i];
            minEndingHere = min(arr[i]*minEndingHere,1);
        }
        else if(arr[i]==0){
            maxEndingHere = 1;
            minEndingHere = 1;
        }
        else {
            int temp = maxEndingHere;
            maxEndingHere = max(arr[i]*minEndingHere,1);
            minEndingHere = temp*arr[i];
        }
        if(maxEndingHere>maxTillHere){
            maxTillHere = maxEndingHere;
        }
    }
    return maxTillHere;
}

int main(){
    int arr[]={1,-1,-4,-22,21,23,46};
    int len = sizeof(arr)/sizeof(arr[0]);
    cout<<maxProd(arr, len-1);
}
