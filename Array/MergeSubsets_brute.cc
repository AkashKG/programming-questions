#include<iostream>
#include<stack>
#include<algorithm>
using namespace std;

typedef struct Interval{
    int start, finish;
}interval;

bool compareInterval(interval i1, interval i2){
    return i1.start<i2.start;
}

void mergeInterval(interval arr[], int len){
    stack<interval> st;
    sort(arr,arr+len,compareInterval);
    st.push(arr[0]);
    for(int i=1;i<len;i++){
        interval top = st.top();
        if(top.finish < arr[i].start)
            st.push(arr[i]);
        else if(top.finish < arr[i].finish){
            top.finish = arr[i].finish;
            st.pop();
            st.push(top);
        }
    }

    while(!st.empty()){
        interval top = st.top();
        cout<<top.start<<" , "<<top.finish<<endl;
        st.pop();
    }
}

int main(){
    int N;
    cin>>N;
    interval in[N];
    for(int i=0;i<N;i++){
        cin>>in[i].start;
        cin>>in[i].finish;
    }
    if(N!=0)
    mergeInterval(in,N);
    return 0;
}
