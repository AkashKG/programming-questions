#include<iostream>
using namespace std;

int val(int arr[], int start, int finish){
    int max = arr[start];
    if(start==finish){
        return max;
    }
    for(int i=start+1;i<finish;i++){
        if(max<arr[i]){max=arr[i];}
    }
    return max;
}

void printMax(int arr[], int len, int K){
    for(int i=0;i<=len-K;i++)
        cout<<val(arr,i,i+K)<<" ";
}

int main(){
    int arr[]={8, 5, 10, 7, 9, 4, 15, 12, 90, 13};
    int k=4;
    printMax(arr,sizeof(arr)/sizeof(arr[0]),k);
    return 0;
}
