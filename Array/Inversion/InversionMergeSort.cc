#include <iostream>
using namespace std;

int merge(int *a, int low, int high, int mid){
    int i, j, k, c[high-low+1], count=0;
    i = low;
    k = 0;
    j = mid + 1;
    while (i <= mid && j <= high){
        if (a[i] < a[j])
            c[k++] = a[i++];
        else{
            c[k++] = a[j++];
            count+=mid+1-i;
        }
    }
    while (i <= mid){
        c[k++] = a[i++];
    }
    while (j <= high){
        c[k++] = a[j++];
    }
    k=0;
    for (i = low; i <= high; i++){
        a[i]=c[k++];
    }
    return count;
}
int mergesort(int *a, int low, int high){
    int mid, count=0;
    if (low < high){
        mid=(low+high)/2;
        count = mergesort(a,low,mid);
        count+= mergesort(a,mid+1,high);
        count+= merge(a,low,high,mid);
    }
    return count;
}

int main(){
    int a[]={3,4,5,0,1,2};
    cout<<mergesort(a, 0, sizeof(a)/sizeof(a[0])-1)<<endl;
}
