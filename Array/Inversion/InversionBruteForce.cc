#include<iostream>
using namespace std;

int countInversion(int arr[], int len){
    int count=0;
    for(int i=0;i<len;i++){
        for(int j=i+1;j<len;j++){
            if(arr[i]>arr[j])
                count++;
        }
    }
    return count;
}

int main(){
    int arr[]={4,5,6,1,2,3};
    cout<<countInversion(arr,sizeof(arr)/sizeof(arr[0]));
    return 0;
}
