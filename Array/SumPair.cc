#include<iostream>
using namespace std;
#define MAX 1000

bool isPossiblePair(int arr[], int len, int sum, bool T[]){
    for(int i=0;i<len;i++){
        if(T[sum-arr[i]] && sum-arr[i]!=arr[i])
            return true;
    }
    return false;
}

int main(){
    bool T[MAX]={false};
    int arr[7];
    for(int i=0;i<7;i++){
        cin>>arr[i];
        T[i]=true;
    }
    int sum; cin>>sum;
    if(isPossiblePair(arr,7,sum, T)){
        cout<<"Possible";
    }
    else
        cout<<"Not Possible";
    return 0;
}
