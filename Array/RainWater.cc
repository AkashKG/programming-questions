#include<iostream>
using namespace std;

int max(int a, int b){return a>b?a:b;}
int min(int a, int b){return a<b?a:b;}

int waterTrapped(int arr[], int len){
    int left[len];
    int right[len], water=0;
    left[0]=arr[0];
    right[len-1]=arr[len-1];
    for(int i=1;i<len;i++){
        left[i]=max(left[i-1],arr[i]);
    }
    for(int i=len-2;i>=0;i--){
        right[i]=max(right[i+1],arr[i]);
    }
    for(int i=0;i<len;i++){
        water += min(left[i],right[i])-arr[i];
    }
    return water;
}

int main(){
    int arr[]={1,0,1};
    int len = sizeof(arr)/sizeof(arr[0]);
    cout<<waterTrapped(arr,len);
    return 0;
}
