#include<iostream>
#include<vector>
using namespace std;

/*
    5,6, 7 ,9, 12, 14
    sum = 26
    output = 4,5
*/

int main(){
    int N, sum, temp_sum;
    cin>>N;
    int v[N];
    for(int i=0;i<N;i++){
            cin>>v[i];
    }
    cin>>sum;
    for(int i=0;i<N;i++){
        temp_sum=v[i];
        for(int j=i+1;j<N;j++){
            if(temp_sum == sum){
                cout<<"The sub array is from "<<i<<" to "<<j-1;
                return 1;
            }
            else if(temp_sum>sum){
                break;
            }
            temp_sum+=v[j];
        }
    }
    return 0;
}
