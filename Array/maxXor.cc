#include<iostream>
using namespace std;

int max(int a, int b){return a>b?a:b;}

int maxXor(int arr[], int len){
    int xorTillHere=0, xorMax=0;
    for(int i=0;i<len;i++){
        xorTillHere = max(xorTillHere, xorTillHere^arr[i]);
        xorMax = max(xorTillHere,xorMax);
    }
    return xorMax;
}

int main(){
    int arr[]={4,5,2,0,2,2,2,1};
    cout<<maxXor(arr,8);
}
