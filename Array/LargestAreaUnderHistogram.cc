#include<iostream>
#include<stack>
using namespace std;

int getMaxArea(int hist[], int len){
    stack<int> s;
    int maxArea = 0, topOfStack, areaWithTop, i=0;
    while(i<len){
        if(s.empty() || hist[s.top()]<hist[i]){
            s.push(i++);
        }
        else{
            topOfStack = s.top();
            s.pop();
            areaWithTop = hist[topOfStack] * (s.empty()?i:i - s.top() - 1);
            if(maxArea < areaWithTop)
                maxArea = areaWithTop;
        }
    }
    while(!s.empty()){
        topOfStack = s.top();
        s.pop();
        areaWithTop = hist[topOfStack] * (s.empty()?i:i - s.top() - 1);
        if(maxArea < areaWithTop)
            maxArea = areaWithTop;
    }
    return maxArea;
}

int main(){
    int arr[]={6, 2, 5, 4, 5, 1, 6};
    cout<<"The maximum area is: "<<getMaxArea(arr,sizeof(arr)/sizeof(arr[0])-1);
    return 0;
}
