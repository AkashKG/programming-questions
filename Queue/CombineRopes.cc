#include<iostream>
#include<queue>
using namespace std;

int minCost(int arr[], int len){
    priority_queue<int, vector<int>, greater<int> > pq(arr,arr+len);
    int res = 0;
    while(pq.size()>1){
        int first = pq.top();
        pq.pop();
        int second = pq.top();
        pq.pop();
        res +=first+second;
        pq.push(first+second);
    }
    return res;
}

int main(){
    int arr[]={4,3,2,6};
    int len = sizeof(arr)/sizeof(arr[0]);
    cout<<minCost(arr,len);
    return 0;
}
