#include <iostream>
using namespace std;

void merge(int *a, int low, int high, int mid){
    int i, j, k, c[high-low+1];
    i = low;
    k = 0;
    j = mid + 1;
    while (i <= mid && j <= high){
        if (a[i] < a[j])
            c[k++] = a[i++];
        else
            c[k++] = a[j++];
    }
    while (i <= mid){
        c[k++] = a[i++];
    }
    while (j <= high){
        c[k++] = a[j++];
    }
    k=0;
    for (i = low; i <= high; i++){
        a[i]=c[k++];
    }
}
void mergesort(int *a, int low, int high){
    int mid;
    if (low < high){
        mid=(low+high)/2;
        mergesort(a,low,mid);
        mergesort(a,mid+1,high);
        merge(a,low,high,mid);
    }
    return;
}

int main(){
    int a[]={3,4,5,0,1,2};
    mergesort(a, 0, sizeof(a)/sizeof(a[0])-1);
    for (int i = 0; i < sizeof(a)/sizeof(a[0]); i++){
        cout<<a[i]<<" ";
    }
}
