#include<iostream>
using namespace std;

void swap(int *a, int *b){
    int t = *a;
    *a = *b;
    *b = t;
}

int partition(int arr[], int low, int high){
    int pivot = arr[high];
    int i=low-1;int j=low;
    for(j;j<high;j++){
        if(arr[j]<=pivot){
            i++;
            swap(&arr[i],&arr[j]);
        }
    }
    swap(&arr[i+1],&arr[high]);
    return i+1;
}

void quickSort(int arr[], int low, int high){
    if(low<high){
        int q = partition(arr, low, high);
        quickSort(arr,low,q-1);
        quickSort(arr,q+1,high);
    }
}

void display(int arr[], int len){
    for(int i=0;i<len;i++)
        cout<<arr[i]<<" ";
}

int main(){
    int arr[]={5,4,3,2,1};
    int len = sizeof(arr)/sizeof(arr[0]);
    quickSort(arr,0, len-1);
    display(arr,len);
    return 0;
}
