//Bubble Sort.
#include<iostream>
using namespace std;

void swap(int &a, int &b){
    a = a^b;
    b = a^b;
    a = a^b;
}

void bubbleSort(int arr[], int len){
    for(int i=0;i<len-1;i++){
        for(int j=i+1;j<len;j++){
            if(arr[i]>arr[j]){
                swap(arr[i],arr[j]);
            }
        }
    }
}

void display(int arr[], int len){
    for(int i=0; i<len; i++){
        cout<<arr[i]<<" ";
    }
}

int main(){
    int arr[]={4,1,21,9,17};
    int len = sizeof(arr)/sizeof(arr[0]);
    bubbleSort(arr, len);
    display(arr, len);
    return 0;
}
