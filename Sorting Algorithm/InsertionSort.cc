#include<iostream>
using namespace std;

void insertionSort(int arr[], int len){
    for(int j=1;j<len;j++){
        int i = j-1;
        int key = arr[j];
        while(i>=0 && key<arr[i]){
            arr[i+1]=arr[i];
            i--;
        }
        arr[i+1]=key;
    }
}

void display(int arr[], int len){
    for(int i=0;i<len;i++)
        cout<<arr[i]<<" ";
}

int main(){
    int arr[]={7,6,5,4,3,2,1};
    int len = sizeof(arr)/sizeof(arr[0]);
    insertionSort(arr, len);
    display(arr,len);
    return 0;
}
