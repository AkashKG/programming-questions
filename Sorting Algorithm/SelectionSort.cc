#include<iostream>
using namespace std;

void swap(int *a, int *b){
    int t = *a;
    *a = *b;
    *b = t;
}

void selection(int arr[], int len){
    int pos ;
    for(int i=0;i<len-1;i++){
        pos = i;
        for(int j=i+1;j<len;j++){
            if(arr[pos]>arr[j])
                pos = j;
        }
        swap(arr[i],arr[pos]);
    }
}

void display(int arr[], int len){
    for(int i=0;i<len;i++)
        cout<<arr[i]<<" ";
}

int main(){
    int arr[]={21,87,91,12,1};
    int len = sizeof(arr)/sizeof(arr[0]);
    selection(arr, len);
    cout<<"\n";
    display(arr, len);
    return 0;
}
