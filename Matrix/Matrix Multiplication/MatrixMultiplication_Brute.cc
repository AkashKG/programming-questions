#include<iostream>
using namespace std;

void multiply(int M[3][3], int N[3][3], int MN[3][3], int len){
    for(int i=0;i<len;i++){
        for(int j=0;j<len;j++){
            MN[i][j]=0;
            for(int k=0;k<len;k++){
                MN[i][j]+=M[i][k]*N[k][j];
            }
        }
    }
}

void display(int MN[3][3], int len){
    for(int i=0;i<len;i++){
        for(int j=0;j<len;j++){
            cout<<MN[i][j]<<" ";
        }
        cout<<"\n";
    }
}

int main(){
    int M[][3]={{
                    1,2,3
                },{
                    4,5,6
                },{
                    7,8,9
                }};
    int N[][3]={{
                    9,2,1
                },{
                    1,5,1
                },{
                    1,8,3
                }};
    int MN[3][3];
    multiply(M,N,MN,3);
    display(MN,3);
    return 0;
}
