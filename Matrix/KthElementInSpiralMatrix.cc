#include<iostream>
using namespace std;
#define MAX 10

int KthPos(int M[4][4], int position, int row, int col){
    int pos = 0;
    int val = -1;
    bool found = false;
    int rowMax = row, colMax = col, currRow = 0, currCol = 0;
    while(rowMax>currRow && colMax >currCol){
        for(int i=currCol;i<colMax;i++){
            pos ++;
            if(pos==position){
                found = true;
                val = M[currRow][i];
                break;
            }
        }
        currRow++;
        if(found)
            break;
        for(int i=currRow;i<rowMax;i++){
            pos++;
            if(pos==position){
                found = true;
                val = M[i][rowMax-1];
                break;
            }
        }
        if(found)
            break;
        colMax--;
        for(int i=colMax-1;i>=currCol;i--){
            pos++;
            if(pos==position){
                found = true;
                val = M[rowMax-1][i];
                break;
            }
        }
        if(found)
            break;
        rowMax--;
        for(int i=rowMax-1;i>=currRow;i--){
            pos++;
            if(pos==position){
                found = true;
                val = M[i][currCol];
                break;
            }
        }
         if(found)
            break;
        currCol++;
    }
    return val;
}

int main(){
    int M[][4]={{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
    cout<<KthPos(M,16,4,4);
    return 0;
}
