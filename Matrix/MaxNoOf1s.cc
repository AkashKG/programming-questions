#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

int getLastPosOfZero(int arr[], int C){
    if(arr[0]==1){
        return -1;
    }
    int pos=0;
    for(int i=0;i<C;i++){
        if(arr[i]==1){
            pos = i-1;
            break;
        }
    }
    return pos;
}

int maxOnes(int M[4][4], int R, int C){
    int max = -1, pos;
    for(int i=0;i<R;i++){
        pos = getLastPosOfZero(M[i], C);
        if(pos == -1){
            max = C;
            break;
        }
        else{
            if(max<C-(pos+1)){
                max = C - pos + 1;
            }
        }
    }
    return max;
}

int main(){
    int arr[4][4]={{0,0,0,1},{0,1,1,1},{0,0,1,1},{0,1,1,1}};
    cout<<maxOnes(arr,4,4);
    return 0;
}
