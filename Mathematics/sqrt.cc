#include<iostream>
using namespace std;

float squareRoot(int num){
    if(num == 0 || num == 1){
       // cout<<"1. Returning number"<<endl;
        return num;
    }
    float result = 0;
    float i=2;
    for(i;num>result;i=i+0.001){
        result = i*i;
        //cout<<i<<" "<<endl;
        if(result==num){
     //       cout<<"2. Returning sqrt"<<endl;
            return i;
        }
    }

   // cout<<"3. Returning sqrt_2"<<endl;
    return i - 0.002;
}

int main(){
    int num=0;
    while(num!=100){
        cout<<num<<" : "<<(float)squareRoot(num)<<endl;
        num ++;
    }

    return 0;
}
