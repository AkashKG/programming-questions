#include<iostream>
using namespace std;

void swap(char *a, char *b){
    char temp;
    temp = *a;
    *a = *b;
    *b = temp;
}

void permute(char arr[],int low, int high){
    int i=0;
    if(low == high){
        cout<<arr<<" ";
        return;
    }
    for(i=low;i<=high;i++){
        swap(arr+low,arr+i);
        permute(arr,low+1,high);
        swap(arr+low,arr+i);
    }
}

int main(){
    char arr[]="Akash";
    int len=5;
    permute(arr, 0, len-1);
    return 0;
}
