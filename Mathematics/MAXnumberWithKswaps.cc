#include<iostream>
#include<string>
using namespace std;

void swap(char *a, char *b){
    char temp;
    temp = *a;
    *a = *b;
    *b = temp;
}

void greatest(string num, int k, string &max){
    if(k==0)
        return;
    for(int i=0;i<num.length();i++){
        for(int j=i+1;j<num.length();j++){
            if(num[i]<num[j]){
                string prev = num;
                swap(&num[i],&num[j]);
                if(num.compare(max)>0){
                    max = num;
                }
                greatest(num,k-1, max);
                swap(&num[i],&num[j]);
            }
        }
    }
}

int main(){
    string number = "11199111";
    int k=1;
    string max = number;
    greatest(number, k, max);
    cout<<max;
    return 0;
}
