#include<iostream>
using namespace std;
const char T[10][5]={"0","1","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};

void print_T9(int digits[], int currDigit, char result[], int size){
    if(currDigit == size){
        cout<<result<<"\n";
        return;
    }
    for(int i=0;T[digits[currDigit]][i]!='\0';++i){
        result[currDigit]=T[digits[currDigit]][i];
        print_T9(digits,currDigit+1, result, size);
    }
}

int main(){
    int digits[]={2,3,4};
    char result[25]={'\0'};
    print_T9(digits,0,result,3);
    return 0;
}
