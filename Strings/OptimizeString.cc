#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;

int computeStrength(int arr[], int len){
    int sum = 0;
    for(int i=0;i<len;i++){
        sum+=arr[i]*arr[i];
    }
    return sum;
}

void display(int arr[], int len){
    for(int i=0;i<len;i++){
        cout<<arr[i]<<" ";
    }
    cout<<endl;
}

int optimize(char arr[], int len, int k){
    int Map[58]={0};
    int l = 58;
    memset(Map, 0, sizeof(Map));
    for(int i=0;arr[i]!='\0';i++){ Map[arr[i]-'A']++; }
    display(Map,l);
    sort(Map, Map+l);
    display(Map,l);
    while(k--){
        Map[l-1]--;
        sort(Map,Map+l);
    }
    display(Map,l);
    return computeStrength(Map, l);
}

int main(){
    char arr[]="Hello";
    int len = sizeof(arr)/sizeof(arr[0]);
    int k = 2;
    cout<<"The value is: "<<optimize(arr, len, k);
    return 0;
}
