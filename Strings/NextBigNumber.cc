#include<iostream>
#include<algorithm>
using namespace std;

void nextNumber(char arr[], int len){
    int i;
    for(i=len-1;i>0;i--){
        if(arr[i-1]<arr[i]){
            break;
        }
    }
    int pos = i-1;
    int smallest = i;
    for(int j = i+1;j<len;j++){
        if(arr[j]>arr[pos]&&arr[j]<arr[smallest]){
            smallest = j;
        }
    }
    swap(arr[pos],arr[smallest]);
    sort(arr+pos+1,arr+len);
}

int main(){
    char number[]="76123";
    nextNumber(number, 5);
    cout<<number;
    return 0;
}
