#include<iostream>
#include<queue>
using namespace std;

int priority(int arr[], int len, int k){
    priority_queue<int, vector<int>, less<int> > pq(arr, arr + len);
    while(k--){
        int top = pq.top();
        pq.pop();
        top--;
        pq.push(top);
    }
    int sum = 0;
    while(pq.size()>0){
        int top = pq.top();
        cout<<top<<" ";
        sum = sum + top*top;
        pq.pop();
    }
    return sum;
}

int main(){
    int arr[]={1,1,1,1,2,2,2,1,1,1,1};
    int k; cin>>k;
    int len=sizeof(arr)/sizeof(arr[0]);
    cout<<priority(arr,len, k);
    return 0;
}
