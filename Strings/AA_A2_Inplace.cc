#include<iostream>
#include<algorithm>
using namespace std;

string toString(int x){
    bool is_negative;
    if (x < 0) { x = -x; is_negative = true;}
    else is_negative = false;
    string s;
    while (x) { s.push_back('0' + x % 10); x /= 10;}
    if (s.empty()) return {"0"};
    if (is_negative) s.push_back('-');
    reverse(s.begin(), s.end());
    return s;
}

int main(){
    string name = "Hello 11111111111Buddy!!";
    int start = 0;
    char distinct = name[0];
    for(int i=1;i<name.length()+1;){
        if(distinct!=name[i]){
            string s = toString(i-start);
            name.insert(start+1,s);
            name.erase(name.begin() + start + 1 + s.length(),name.begin() + s.length() + i);
            i=start+s.length()+1;
            start=i;
            distinct=name[start];
            continue;
        }
        i++;
    }
    cout<<name;
}
