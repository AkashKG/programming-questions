#include<iostream>
#include<string>
#include<sstream>
using namespace std;

void replaceWithSingle(string &s1, string &s2, int i){
    int count=0;
    if(s1[i+1]==s2[i]){
        for(int j=i;;j++){
            if(s2[i]==s2[j])
                count++;
            else
                break;
        }
        for(int j=i+1;;j++){
            if(s1[j]==s1[i+1])
                count--;
            else
                break;
        }
        if(count==0)
            s1.erase(s1.begin() + i);
        else
            s1[i]=s2[i];
    }
    else{
        s1[i]=s2[i];
    }
}

void replaceWithPrevious(string &s1, string &s2, int i){
    string sub_s="";
    if((s1.substr(0,i-1))==s2.substr(0,i-1)){
        if(s2[i]==s1[i-1]){
            for(int k=i;;k++){
                if(s2[i]==s2[k]){
                    sub_s = sub_s + s2[i];
                }
                else{
                    break;
                }
            }
            s1.erase(s1.begin()+i);
            s1.insert(i,sub_s);
        }
        else{
            s1.erase(s1.begin()+i);
        }
    }
}

void replaceAnySequence(string &s1, string &s2, int i){
    if(s2[i]==s1[i+1]){
        s1.erase(s1.begin()+i);
    }
}

bool possibleToConvert(string &str_mutant, string str_final){
    for(int i=0;i<str_mutant.length();){
        if(str_mutant[i]==str_final[i]){i++;}
        else{
            switch(str_mutant[i]){
            case '*':replaceAnySequence(str_mutant, str_final, i);
                break;
            case '?':replaceWithSingle(str_mutant,str_final,i);
                break;
            case '+':replaceWithPrevious(str_mutant, str_final,i);
                break;
            }
        }
    }
    cout<<str_mutant<<endl;
    return str_mutant == str_final;
}

int main(){
    string str_mutant, str_final;
    cin>>str_mutant>>str_final;
    if(possibleToConvert(str_mutant, str_final))
        cout<<"True";
    else
        cout<<false;
    return 0;
}
