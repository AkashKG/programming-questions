#include<iostream>
#include<string>
using namespace std;

bool ifPalin(string number){
    int start = 0, last = number.length()-1;
    while(start<last){
        if(number[start]!=number[last])
            return false;
        start++;
        last--;
    }
    return true;
}

bool convertToPalin(string &number, int k){
    int len = number.length();
    int start = 0, last = len-1;
    while(start<last&&k>0){
        if(number[start]==number[last]){start++;last--;}
        else{number[start]=number[last]; start++;last--; k--;}
    }
    return ifPalin(number);
}

int main(){
    string number="1111";
    int k=2;
    if(convertToPalin(number, k)){
        cout<<number;
    }
    else{
        cout<<-1;
    }
    return 0;
}
