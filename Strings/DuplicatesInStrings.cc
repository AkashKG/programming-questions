#include<iostream>
#include<set>
using namespace std;

void removeDuplicates(char str[], int len){
    set<char> uniqueSet;//to store the unique elements from the string.
    for(int i=0;i<len;i++){
        uniqueSet.insert(str[i]);
    }
    set<char>::iterator i=uniqueSet.begin();
    i++;
    int pos = 0;
    for(i;i!=uniqueSet.end();i++){//fetching from the set. Order Not maintained.
        str[pos++]=*i;
    }
    for(int j=pos;j<len;j++){//Next all positions to '\0'
        str[j]='\0';
    }
}

int main(){
    char str[]="HelloBuddy!";
    removeDuplicates(str, sizeof(str)/sizeof(str[0]));
    cout<<str;
    return 0;
}
