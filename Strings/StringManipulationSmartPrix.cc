#include<iostream>
#include<string>
#include<sstream>
using namespace std;
#define MAX_1 5
#define MAX_2 9

void display(string s[], int len){
    for(int i=0;i<len;i++)
        cout<<s[i]<<" ";
    cout<<endl;
}

void man(string s[], string m[]){
    int pos=0;
    for(int i=0;i<MAX_2;i++){
        size_t start=m[i].find_first_of("{");
        size_t last=m[i].find_first_of("}");
        if(start+1==last && start!=-1){
            m[i]=s[pos];
            pos++;
        }
        else{
            if(start!=-1){
                int number;
                istringstream buffer(m[i].substr(start+1,last-1));
                buffer >> number;
                m[i]=s[number];
            }
        }
    }
}

int main(){
    string str[MAX_1];
    for(int i=0;i<MAX_1;i++){
        cin>>str[i];
    }
    string str_main[MAX_2];
    for(int i=0;i<MAX_2;i++){
        cin>>str_main[i];
    }
    man(str,str_main);
    display(str_main,MAX_2);
    return 0;
}
