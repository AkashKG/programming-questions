#include<iostream>
#include<set>
#include<string>
using namespace std;

void removeDuplicates(string &str, int len){
    set<char> s;
    for(int i=0;i<len;){
        if(s.find(str[i]) == s.end()){
            s.insert(str[i]);
            i++;
        }
        else{
            str.erase(str.begin()+i);
        }
    }
    cout<<str;
}

int main(){
    string str ("ThisProgramIsForYou!!");
    removeDuplicates(str,str.length());
    return 0;
}
