#include<iostream>
#include<string>
#include<algorithm>
using namespace std;

string lexMin(string str){
    int len = str.length();
    string str_arr[len];
    string str_str = str + str;
    for(int i=0;i<len;i++){
        str_arr[i] = str_str.substr(i,len);
    }
    sort(str_arr, str_arr+len);
    return str_arr[0];
}

int main(){
    string str="QUORA";
    cout<<lexMin(str);
    return 0;
}
