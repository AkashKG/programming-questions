#include<iostream>
#include<stack>
using namespace std;

bool isBalanced(char arr[],int len){
    stack<char> st;
    for(int i=0;i<=len;i++){
        if(arr[i]=='a'){
            st.push(arr[i]);
            cout<<"Pushed: "<<arr[i]<<"\n";
        }
        else{
            if(arr[i]=='b'&& !st.empty()){
                    st.pop();
                    cout<<"Popped: "<<arr[i]<<"\n";
            }
            else if(arr[i]=='b'&&st.empty()){
                return false;
            }
        }
    }
    if(st.empty())
        return true;
    return false;
}

int main(){
    char ab[]="bbaabb";
    if(isBalanced(ab, sizeof(ab)/sizeof(ab[0])-1)){
        cout<<"Good String";
    }
    else
        cout<<"Bad String";
    return 0;
}
