#include<iostream>
#include<malloc.h>
using namespace std;

typedef struct Node{
    int data;
    struct Node *next;
}*node;
node insertIntoList(int data, node start);
node sum(node m, node n, node sumNum){
    int data, carry = 0;
    while(m!=NULL && n!=NULL){
        data = m->data + n->data + carry;
        if(data >= 10){
            carry = 1;
            data = data % 10;
            sumNum = insertIntoList(data, sumNum);
        }
        else{
            sumNum = insertIntoList(data, sumNum);
            carry = 0;
        }
        m = m->next;
        n = n->next;
    }
    while(m!=NULL){
        data = m->data + carry;
        if(data >=10){
            carry = 1;
            sumNum = insertIntoList(data%10, sumNum);
        }
        else{
            sumNum = insertIntoList(data, sumNum);
            carry = 0;
        }
        m=m->next;
    }
    while(n!=NULL){
        data = n->data + carry;
        if(data >=10){
            carry = 1;
            sumNum = insertIntoList(data%10, sumNum);
        }
        else{
            sumNum = insertIntoList(data, sumNum);
            carry = 0;
        }
        n=n->next;
    }
    if(m==NULL && n==NULL && carry == 1){
        sumNum = insertIntoList(carry, sumNum);
    }
    return sumNum;
}

void display(node start){
    cout<<endl;
    node temp = start;
    while(temp!=NULL){
        cout<<temp->data;
        temp = temp ->next;
    }
    cout<<endl;
}

node reverseList(node *start){
    node first = *start;
    node rest = first->next;
    if(rest == NULL){
        return *start;
    }
    reverseList(&rest);
    first->next->next = first;
    first->next=NULL;
    *start = rest;
}

node insertIntoList(int data, node start){
    node newnode = (node)malloc(sizeof(struct Node));
    newnode->data = data;
    if(start == NULL){
        newnode->next = NULL;
        start = newnode;
        return start;
    }
    node temp = start;
    while(temp->next != NULL)
        temp=temp->next;
    newnode->next = NULL;
    temp->next = newnode;
    return start;
}

int main(){
    node num1 = NULL, num2 = NULL;
    int data;
    cin>>data;
    while(data>0){
        num1 = insertIntoList(data%10, num1);
        data/=10;
    }
    display(num1);
    cin>>data;
    while(data>0){
        num2 = insertIntoList(data%10, num2);
        data/=10;
    }
    display(num2);
    node num3 = NULL;
    num3 = sum(num1, num2, num3);
    reverseList(&num1);
    reverseList(&num2);
    reverseList(&num3);
    display(num1);
    cout<<" + ";
    display(num2);
    cout<<" = ";
    display(num3);
    return 0;
}
