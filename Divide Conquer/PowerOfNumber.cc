#include<iostream>
using namespace std;

int power(int coeff, int p){
    if(p==1)
        return coeff;
    if(p%2!=0)
        return power(coeff,(p+1)/2)*power(coeff,p/2);
    return power(coeff,p/2)*power(coeff,p/2);
}

int main(){
    int coeff=2,p=10;
    cout<<power(coeff,p);
    return 0;
}
